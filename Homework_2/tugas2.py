import torch
from torch.autograd import Variable
import cv2
from data import BaseTransform, VOC_CLASSES as labelmap
from ssd import build_ssd
import imageio
import os

def detect(frame, net, transform):
    def_color = (255, 0, 0)
    height, width = frame.shape[:2]
    frame_t = transform(frame)[0]
    x = torch.from_numpy(frame_t).permute(2, 0, 1)
    x = Variable(x.unsqueeze(0))
    y = net(x)
    detections = y.data
    scale = torch.Tensor([width, height, width, height])

    for i in range(detections.size(1)):
        j = 0
        while detections[0, i, j, 0] >= 0.6:
            pt = (detections[0, i, j, 1:] * scale).numpy()
            point1 = (int(pt[0]), int(pt[1]))
            point2 = (int(pt[2]), int(pt[3]))
            
            cv2.rectangle(frame, point1 , point2 , def_color , 2)
            cv2.putText(frame, labelmap[i - 1], point1, cv2.FONT_HERSHEY_SIMPLEX, 2, def_color, 2)
            j += 1

    return frame

net = build_ssd('test')
net.load_state_dict(torch.load('ssd300_mAP_77.43_v2.pth', map_location = lambda storage, loc: storage))
transform = BaseTransform(net.size, (104/256.0, 117/256.0, 123/256.0))

path = ".\\original"
save_path = ".\\result\\"
data =  os.listdir(path)

for video in data:
    #reader = imageio.get_reader('epic_horses.mp4')
    reader = imageio.get_reader(video)
    fps = reader.get_meta_data()['fps']
    output_file = save_path + "hasil-" + video
    writer = imageio.get_writer(output_file, fps = fps)
    for i, frame in enumerate(reader):
        frame = detect(frame, net.eval(), transform)
        writer.append_data(frame)
        print(i)
    writer.close()

