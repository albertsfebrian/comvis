import cv2

face_cas = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cas = cv2.CascadeClassifier('haarcascade_eye.xml')
smile_cas = cv2.CascadeClassifier('haarcascade_smile.xml')

def detection(gray, frame):
    face = face_cas.detectMultiScale(gray, 1.3 , 5)
    for(x, y, w, h) in face:
        text = " Smile Please :)"
        def_color = (0,0,255)

        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        
        # eye = eye_cas.detectMultiScale(roi_gray, 1.1 , 20)
        # for (ex, ey, ew, eh) in eye:
        #     cv2.rectangle(roi_color, (ex,ey), (ex+ew , ey+eh), (0,255,0), 2)

        smile = smile_cas.detectMultiScale(roi_gray, 1.7 ,20)
        for (sx, sy, sw, sh) in smile:
            text = "Smile Detected"
            cv2.rectangle(roi_color, (sx,sy), (sx+sw , sy+sh), (255,0,0), 2)
            def_color = (0,165,255)
        

        cv2.putText(frame, text, (x, y - 10),cv2.FONT_HERSHEY_SIMPLEX, 1, def_color, 2)
        cv2.rectangle(frame, (x,y), (x+w, y+h), def_color , 2 )
    return frame

webcam = cv2.VideoCapture(0)
while True:
    _,frame = webcam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    show = detection(gray, frame)
    cv2.imshow('Homework', show)
    if(cv2.waitKey(1) & 0xFF == ord('q')):
        break
webcam.release()
cv2.destroyAllWindows()
